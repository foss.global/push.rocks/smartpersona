import * as plugins from './smartpersona.plugins';
import { foldDec } from '@pushrocks/smartjson';

export class Company extends plugins.smartjson.Smartjson implements plugins.tsclass.ICompany {
  @foldDec()
  public foundedDate: plugins.tsclass.IDate;
  
  @foldDec()
  public closedDate: plugins.tsclass.IDate;
  
  @foldDec()
  public name: string;
  
  @foldDec()
  public status: plugins.tsclass.TCompanyStatus;

  @foldDec()
  public contact: plugins.tsclass.IContact;

  constructor() {
    super();
  }
}